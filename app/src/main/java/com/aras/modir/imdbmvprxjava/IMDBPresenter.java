package com.aras.modir.imdbmvprxjava;

import com.aras.modir.imdbmvprxjava.pojo.IMDBPojo;
import com.aras.modir.imdbmvprxjava.pojo.Rating;

public class IMDBPresenter implements IMDBContract.Presenter {
    private IMDBContract.View view;
    IMDBContract.Model model = new IMDBModel();

    @Override
    public void attatchView(IMDBContract.View view) {
        this.view = view;
        model.attatchPresenter(this);
    }

    @Override
    public void searchByWord(String word) {
        model.search(word);
        view.onDataLoading();
    }

    @Override
    public void receivedDataSuccess(IMDBPojo result) {
        view.showSuccessData(result);
        view.onDataLoadingFinished();
    }

    @Override
    public void unFailure(String msg) {
        view.unFailure(msg);
        view.onDataLoadingFinished();
    }

    @Override
    public void onSelectForcast(Rating ratinglist) {
        view.showForcastData(ratinglist);
    }
}
