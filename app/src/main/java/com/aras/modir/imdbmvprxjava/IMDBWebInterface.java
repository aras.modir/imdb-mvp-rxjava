package com.aras.modir.imdbmvprxjava;

import com.aras.modir.imdbmvprxjava.pojo.IMDBPojo;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IMDBWebInterface {
    @GET("/")
    Observable<IMDBPojo>
    searchInIMDB(@Query("t") String t, @Query("apikey") String apikey);
}
