package com.aras.modir.imdbmvprxjava;

import com.aras.modir.imdbmvprxjava.pojo.IMDBPojo;
import com.aras.modir.imdbmvprxjava.pojo.Rating;

public interface IMDBContract {

    interface View {
        void showSuccessData(IMDBPojo result);

        void unFailure(String msg);

        void onDataLoading();

        void onDataLoadingFinished();

        void showForcastData(Rating ratinglist);
    }

    interface Presenter {
        void attatchView(View v);

        void searchByWord(String word);

        void receivedDataSuccess(IMDBPojo result);

        void unFailure(String msg);

        void onSelectForcast(Rating ratinglist);
    }

    interface Model {
        void attatchPresenter(Presenter presenter);

        void search(String word);
    }
}
