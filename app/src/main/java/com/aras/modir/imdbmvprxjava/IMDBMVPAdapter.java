package com.aras.modir.imdbmvprxjava;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aras.modir.imdbmvprxjava.pojo.Rating;

import java.util.List;

public class IMDBMVPAdapter extends RecyclerView.Adapter<IMDBMVPAdapter.Holder> {

    Context mContext;
    List<Rating> list;

    public IMDBMVPAdapter(Context mContext, List<Rating> list) {
        this.mContext = mContext;
        this.list = list;
    }

    @Override
    public IMDBMVPAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.recycleview_item_imdb, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull IMDBMVPAdapter.Holder holder, int i) {
        holder.source.setText(list.get(i).getSource());
        holder.value.setText(list.get(i).getValue());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView source;
        TextView value;

        public Holder(@NonNull View itemView) {
            super(itemView);
            source = itemView.findViewById(R.id.source);
            value = itemView.findViewById(R.id.value);
            source.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClick.onItemClick(list.get(getAdapterPosition()));
                }
            });
        }
    }

    onItemClick onItemClick;

    public void setOnItemClick(IMDBMVPAdapter.onItemClick onItemClick) {
        this.onItemClick = onItemClick;
    }

    public interface onItemClick {
        void onItemClick(Rating ratinglist);
    }
}
