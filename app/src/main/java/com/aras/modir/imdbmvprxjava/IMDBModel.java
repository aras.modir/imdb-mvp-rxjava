package com.aras.modir.imdbmvprxjava;

import android.util.Log;

import com.aras.modir.imdbmvprxjava.pojo.IMDBPojo;

import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IMDBModel implements IMDBContract.Model {
    private IMDBContract.Presenter presenter;

    @Override
    public void attatchPresenter(IMDBContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void search(String word) {

        API.getMovie().create(IMDBWebInterface.class)
                .searchInIMDB(word, "70ad462a").subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<IMDBPojo>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(IMDBPojo imdbPojo) {
                        presenter.receivedDataSuccess(imdbPojo);

                    }

                    @Override
                    public void onError(Throwable e) {
                        presenter.unFailure(e.toString());

                    }

                    @Override
                    public void onComplete() {

                    }
                });

//        API.getMovie().create(IMDBWebInterface.class).searchInIMDB(word, "70ad462a")
//                .enqueue(new Callback<IMDBPojo>() {
//                    @Override
//                    public void onResponse(Call<IMDBPojo> call, Response<IMDBPojo> response) {
//                        Log.d("Message", "Actor: " + response.body().getActors());
//                        presenter.receivedDataSuccess(response.body());
//                    }
//
//                    @Override
//                    public void onFailure(Call<IMDBPojo> call, Throwable t) {
//                        presenter.unFailure(t.toString());
//                    }
//                });
    }
}
